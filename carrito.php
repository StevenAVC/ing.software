<?php
    /* INICIAR SESIÓN */
    session_start();

    /* METODO PARA ENTRAR SOLO CON INICIO DE SESION */
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: index.php");
        exit;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carrito Compras</title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://kit.fontawesome.com/07570749ac.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>
    <script src="js/carrito.js"></script>
</head>
<body style="background-color:#EDFEFF">
    <?php include('MenuNav.php') ?>
    

    <?php
        require('conDB.php');

        $resultado = $mysqlconn->query("SELECT * FROM catalogo") or die($mysqlconn->error);
    ?>
    
    <?php require_once '../Pijamas/conexiones/carritoProcesos.php' ?>
<!--- METODO PARA MOSTRAR MENSAJES DE ACCIONES --->
<?php if(isset($_SESSION['mensaje'])): ?>
        <div class="alert alert-success">
            <?php 
                echo $_SESSION['mensaje'];
                /* LIMPIAR MENSAJE */
                unset($_SESSION['mensaje']);
            ?>
        </div>
    <?php  endif; ?>
<!--- FIN DEL METODO DE MOSTRAR MENSAJES DE ACCIONES --->

    <div class="container">
        <br><br><br>
        <h1 align="center">Realiza tu compra</h1>
        <br>
        <form action="../Pijamas/conexiones/carritoProcesos.php" method="POST" id="carritoForm">
            <input type="hidden" id="id" name="id" value="">

            <div class="form-group">
                <label for="id_pijama">Pijama</label>
                <select name="id_pijama" id="id_pijama" class="form-control">
                    <option value="">Selecciona un producto</option>
                    <?php
                        while($dato = $resultado->fetch_assoc()){
                            $idCata = $dato['id'];
                            $nombreCata = $dato['nombre'];
                            $selected = "";
                            if($idCata == $id_pijama) {
                                $selected = "selected";
                            }
                            echo "<option $selected value='$idCata'>$nombreCata</option>";
                        }
                    ?>
                </select>
                <!--- Input que almacena el ID del usuario logeado--->
                <?php
                    $idUser = $_SESSION["id"];
                    echo "<input hidden name='id_usuario' id='id_usuario' value='$idUser'>";
                ?>
                
                <br>
            </div>
            <br>
            <div class="from-group">
                <button type="submit" id="btnGuardar" name="btnGuardar" class="form-control btn btn-primary">Guardar</button>
            </div>
        </form>

        <br>
        <?php 

            /*Variable que almacena el ID del usuario logeado */
            $idUsuario = $_SESSION["id"];

            /* ENVIO DE DATOS A LA VARIABLE $resultadoC */
            $resultadoC = $mysqlconn->query("SELECT tC.id idCompra, tCat.nombre nombreProducto, tCat.precio precioProducto, tCat.descripcion, tUsu.usuario, tUsu.email "
            . " FROM compras tC"
            . " INNER JOIN catalogo tCat ON
            tC.idCatalogo = tCat.id"
            . " INNER JOIN usuarios tUsu ON
            tC.idUsuario = tUsu.id"
            . " Where tC.idUsuario = '$idUsuario'") or die($mysqlconn->error);
        ?>
        <h2>Lista de Compra</h2>
        <div class="row">
            <table class="table">
                    <tr>
                        <th>id</th>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Descripción</th>
                        <th>Usuario</th>
                        <th>Correo Electrónico</th>
                        <th>Acciones</th>
                    </tr>
                <?php 
                /* fetch_assoc SIRVE PARA PASAR A LA SIGUIENTE LINEA */
                /* GUARDAR LOS DATOS DE RESULTADO EN LA VARIABLE DATOS */
                while($dato = $resultadoC->fetch_assoc()): 
                ?>
                <tr>
                    <td><?php echo $dato['idCompra'] ?></td>
                    <td><?php echo $dato['nombreProducto'] ?></td>
                    <td><?php echo $dato['precioProducto'] ?></td>
                    <td><?php echo $dato['descripcion'] ?></td>
                    <td><?php echo $dato['usuario'] ?></td>
                    <td><?php echo $dato['email'] ?></td>
                    <td>
                        <a class="btn btn-danger" href="../Pijamas/conexiones/carritoProcesos.php?borrar=<?php echo $dato['idCompra'] ?>">Borrar</a>
                    </td>
                </tr>
                <?php 
                    /* FINAL CICLO WHILE */
                    endwhile; 
                ?>
            </table>
        </div>
    </div>
</body>
</html>