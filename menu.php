<?php
    /* INICIAR SESIÓN */
    session_start();

    /* METODO PARA ENTRAR SOLO CON INICIO DE SESION */
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: index.php");
        exit;
}

?>

<!DOCTYPE html>

<html> 
    <head> 
         <?php include("./MenuNav.php") ?>
         <link rel="stylesheet" href="css/footer.css">
        <script src="https://kit.fontawesome.com/07570749ac.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    </head>
    <body> 
        <div class="container">
            <br>
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                     Pijamas de Adulto.
                    </button>
                  </h2>
                  <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="adulto//adulto1.jpeg" alt="Pijamas de adulto" width="350" height="280">
                                <h5>Talla L</h5>
                                <h6>₡8500</h6>
                            </div>
                            <div class="col-md-4">
                                <img src="adulto/adulto2.jpeg" alt="Pijamas de Adulto" width="350" height="280">
                                <h5>Talla M</h5>
                                <h6>₡8500</h6>
                            </div>
                            <div class="col-md-4">
                                <img src="adulto/adulto3.jpg" alt="Pijamas de Adultos" width="350" height="280">
                                 <h5>Talla XL</h5>
                                 <h6>₡10000</h6>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>  
                
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                     Pijamas de Niña.
                    </button>
                  </h2>
                  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                        <div class="col-md-4">
                            <img src="niñas//Niña1.jpeg" alt="Pijama de Niña" width="350" height="280">
                            <h5>Talla 6</h5>
                            <h6>₡7000</h6>
                        </div>
                        <div class="col-md-4">
                            <img src="niñas//Niña2.jpeg" alt="Pijama de Niña" width="350" height="280">
                            <h5>Talla 8</h5>
                            <h6>₡7000</h6>
                        </div>
                        <div class="col-md-4">
                            <img src="niñas//Niña3.jpeg" alt="Pijama de Niña" width="350" height="280">
                            <h5>Talla 4</h5>
                            <h6>₡7000</h6>
                        </div>
                        </div>
                    </div>
                  </div>
                </div>
                
                 <div class="accordion-item">
                 <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                     Pijamas de Niño.
                    </button>
                  </h2>
                  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                        <div class="col-md-4">
                            <img src="niños//niño1.jpeg" alt="Pijama de Niño" width="350" height="280">
                            <h5>Talla 6</h5>
                             <h6>₡7000</h6>
                        </div>
                        <div class="col-md-4">
                            <img src="niños//niño2.jpeg" alt="Pijama de Niño" width="350" height="280">
                            <h5>Talla 4</h5>
                             <h6>₡7000</h6>
                        </div>
                        <div class="col-md-4">
                            <img src="niños//niño3.jpeg" alt="Pijama de Niño" width="350" height="280">
                            <h5>Talla 10</h5>
                             <h6>₡7000</h6>
                        </div>
                        </div>
                    </div>
                  </div>
                </div> 
            </div>            
        </div>
        <br>
        <br>
        <br>
        <br>
<?php
    include('footer.html');
?>
    </body>
</html>
