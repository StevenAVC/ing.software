<?php
    /* INICIAR SESIÓN */
    session_start();

    /* METODO PARA ENTRAR SOLO CON INICIO DE SESION */
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: index.php");
        exit;
}

?>

<!DOCTYPE html>

<html> 
    <head> 
        <?php include("./MenuNav.php") ?>
        <link rel="stylesheet" href="css/footer.css">
        <script src="https://kit.fontawesome.com/07570749ac.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    </head>
    <body> 
        <div class="container" style="margin-top: 50px;">
            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="img/Pijama-Capri-Niña-PROVOCAME-Guau--Talla-6-8.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="https://cuidading.com/wp-content/uploads/2017/08/los-mejores-pijamas-hombre.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="https://ae01.alicdn.com/kf/Hd9072c288da843d7a5f7a1f4cdbff4c3q/2019-ni-os-Pijama-conjunto-ni-os-Pijamas-Mickey-Pijama-beb-ni-o-Pijamas-de-Navidad.jpeg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <center><h1>Calidad garantizada</h1></center>
                </div>
                <div class="col-sm-6">
                    <center><h1>Para todas las edades</h1></center>
                </div>

            </div>
        </div>
<?php
    include('footer.html');
?>
    </body>
</html>