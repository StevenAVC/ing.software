<?php
    require "conexiones/sistemaLogeo.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/login.css">
    <title>Logeo | LuMaJi</title>
</head>
<body>
        <div class="container-todo">
        <div class="container-form">
            <img src="img/person.png" alt="" class="logo">
            <h1 class="titulo">Iniciar Sesión</h1>

            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                <label for="">Correo Electrónico</label>
                <input type="text" name="email">
                <span class="msg-alerta"><?php echo $email_error; ?></span>
                <label for="">Contraseña</label>
                <input type="password" name="password">
                <span class="msg-alerta"><?php echo $password_error; ?></span>
                <input type="submit" value="Iniciar Sesión">

            </form>

            <span class="logeo-footer">¿Aún no tienes cuenta?
                <a href="registro.php">Registrate</a>
            </span>
        </div>
        <div class="capa1"></div>
        <div class="container-texto">
            <div class="capa"></div>
            <h1 class="titulo-desc">¡Bienvenid@ a la mejor tienda de ropa!.</h1>
            <p class="texto-desc">Te brindaremos una gran variedad de artículos de buena calidad y precio el cual está basado en todas las necesidades de los clientes.</p>
        </div>
    </div>

    <div class="copyright">Derechos Reservados &copy;Pijamas LuMaJi 2021</div>
</body>
</html>