<?php
    /* INICIAR SESIÓN */
    session_start();

    /* METODO PARA ENTRAR SOLO CON INICIO DE SESION */
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: index.php");
        exit;
}

?>

<!DOCTYPE html>

<html> 
    <head> 
         <?php include("./MenuNav.php") ?>
         <link rel="stylesheet" href="css/footer.css">
        <script src="https://kit.fontawesome.com/07570749ac.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    </head>
    <body> 
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <br>
                    <div class="card" style="width: 17rem;">
                        <img src="img/Dueña.jpg" class="card-img-top" alt="..."  width="180px" height="280px">
                        <div class="card-body">
                          <p class="card-text">Dueña de la tienda Pijamas LuMaJi</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <br>
                    <div class="card" style="width: 17rem;">
                        <img src="img/Empleado del mes.jpg" class="card-img-top" alt="..."  width="180px" height="280px">
                        <div class="card-body">
                          <p class="card-text">El mas productivo.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <br>
                    <div class="card" style="width: 17rem;">
                        <img src="img/Novato.jpg" class="card-img-top" alt="..."  width="180px" height="280px">
                        <div class="card-body">
                          <p class="card-text">Novato destacado.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
<?php
    include('footer.html');
?>
    </body>
</html>



