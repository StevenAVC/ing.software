<?php
        
    /* ARCHIVO CONEXION MYSQL */
    require_once "../Pijamas/conexiones/ConBD.php";

    /* DEFINICION VARIABLES */
    $usuario = $email = $password = "";
    $usuario_error = $email_error = $password_error = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        

    /* VALIDACION NOMBRE USUARIO */    
        /* VALIDACION SI ESTA VACIO O NO EL USUARIO */
        if(empty(trim($_POST["usuario"]))){
            $usuario_error = "Por favor, ingrese un nombre de usuario";
        }else{
            /* COMANDOS SQL */
            $sql = "SELECT ID FROM usuarios WHERE usuario = ?";

            if($stmt = mysqli_prepare($conexion, $sql)){
                mysqli_stmt_bind_param($stmt, "s", $param_usuario);

                /*METODO PARA LIMPIAR CASILLA*/
                $param_usuario = trim($_POST["usuario"]);

                if(mysqli_stmt_execute($stmt)){
                    mysqli_stmt_store_result($stmt);
                    
                    /* VALIDACION SI EXISTE EL USUARIO */
                    if(mysqli_stmt_num_rows($stmt) == 1){
                        $usuario_error = "Este nombre de usuario ya está registrado";
                    }else{
                        $usuario = trim($_POST["usuario"]);
                    }
                }else{
                    /* MENSAJE DE ERROR DE VALIDACION */
                    echo "Hubo un error en el sistema, inténtalo mas tarde :(";
                }
            }
        }

    /* VALIDACION CORREO ELECTRONICO */    
        /* VALIDACION SI ESTA VACIO O NO EL EMAIL */
        if(empty(trim($_POST["email"]))){
            $email_error = "Por favor, ingresa un correo electronico";
        }else{
            /* COMANDOS SQL */
            $sql = "SELECT ID FROM usuarios WHERE email = ?";

            if($stmt = mysqli_prepare($conexion, $sql)){
                mysqli_stmt_bind_param($stmt, "s", $param_email);

                /*METODO PARA LIMPIAR CASILLA*/
                $param_email = trim($_POST["email"]);

                if(mysqli_stmt_execute($stmt)){
                    mysqli_stmt_store_result($stmt);
                    
                    /* VALIDACION SI EXISTE EL EMAIL */
                    if(mysqli_stmt_num_rows($stmt) == 1){
                        $email_error = "Este email ya está registrado";
                    }else{
                        $email = trim($_POST["email"]);
                    }
                }else{
                    /* MENSAJE DE ERROR DE VALIDACION */
                    echo "Hubo un error en el sistema, inténtalo mas tarde :(";
                }
            }
        }

    /* VALIDACION CONTRASEÑA */
        if(empty(trim($_POST["password"]))){
            $password_error = "Por favor, ingresa una contraseña";
        }elseif(strlen(trim($_POST["password"])) < 4){
            $password_error = "La contraseña debe de tener al menos 4 caracteres";
        }else{
            $password = trim($_POST["password"]);
        }

        /* COMPROBACION DE ERRORES ENTRADA */
        if(empty($usuario_error) && empty($email_error) && empty($password_error)){
            /* CREACION STATEMENTS */
            $sql = "INSERT INTO usuarios (usuario, email, contrasena) VALUES (?, ?, ?)";

            if($stmt = mysqli_prepare($conexion, $sql)){
                mysqli_stmt_bind_param($stmt, "sss", $param_usuario, $param_email, $param_password);

                /* ESTABLECER PARAMETROS */
                $param_usuario = $usuario;
                $param_email = $email;
                $param_password = password_hash($password, PASSWORD_DEFAULT); /* CONTRASEÑA ENCRIPTADA */

                if(mysqli_stmt_execute($stmt)){
                    header("location: index.php");
                }else{
                    echo "Hubo un error en el registro, intentalo despues :(";
                }
            }
        }

        mysqli_close($conexion); /* CERRAR SESIÓN DEL PROCESO */
    }

?>