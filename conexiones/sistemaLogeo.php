<?php

    //INICIALIZAR LA SESION
    session_start();
    
    /* MANTENER SESION INICIADA */
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
        header("location: inicio.php");
        exit;
    }

    /* ARCHIVO CONEXION MYSQL */
    require_once "../Pijamas/conexiones/ConBD.php";

    /* DEFINICION VARIABLES */
    $email = $password ="";
    $email_error = $password_error = "";

    if($_SERVER["REQUEST_METHOD"] === "POST"){
        
    /* VALIDACION EMAIL */    
        /* VALIDACION SI ESTA VACIO O NO EL EMAIL */
        if(empty(trim($_POST["email"]))){
            $email_error = "Por favor, ingrese el correo electronico";
        }else{ /* SI NO ESTA VACIO SE GUARDA AQUI */
            $email = trim($_POST["email"]);
        }
    /* VALIDACION CONTRASEÑA */    
        /* VALIDACION SI ESTA VACIO O NO LA CONTRASEÑA */
        if(empty(trim($_POST["password"]))){
            $password_error = "Por favor, ingrese una contraseña";
        }else{ /* SI NO ESTA VACIO SE GUARDA AQUI */
            $password = trim($_POST["password"]);
        }

    /* VALIDACION DE DATOS */    
        /* VALIDACION SI ESTA VACIO O NO LOS DATOS */
        if(empty($email_error) && empty($password_errpr)){
            
            $sql = "SELECT id, usuario, email, contrasena FROM usuarios WHERE email = ?";
            
            if($stmt = mysqli_prepare($conexion, $sql)){
                
                mysqli_stmt_bind_param($stmt, "s", $param_email);
                
                $param_email = $email;
                
                if(mysqli_stmt_execute($stmt)){ /* EJECUCION DEL STATEMENT */
                    mysqli_stmt_store_result($stmt); /* CAPTURA RESULTADOS STATEMENT */
                }
                /* VALIDACION SI EXISTE EL EMAIL */
                if(mysqli_stmt_num_rows($stmt) == 1){
                    mysqli_stmt_bind_result($stmt, $id, $usuario, $email, $hashed_password);
                    /* SI ENCUENTRA EL USUARIO SE REALIZA EL METODO */
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            session_start();
                            
                            /* VARIABLE DE SESION */
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["email"] = $email;
                            
                            header("location: inicio.php");
                        }else{
                            $password_error = "La contraseña está incorrecta";
                        }
                    } 
                }else{
                    $email_error = "Este Email no existe";
                }
                
            }else{
                echo "Hubo un error en el sistema, inténtalo mas tarde :(";
            }
        }
        
        mysqli_close($conexion); /* CERRAR SESIÓN DEL PROCESO */
        
    }

?>