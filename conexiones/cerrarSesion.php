<?php
    /* INICIAR SESIÓN */
    session_start();

    $_SESSION = array();
    /* METODO PARA ELIMINAR LA SESION */
    session_destroy();

    /* REDIRECCION DE PAGINA */
    header("location: ../");

    /* TERMINAR LA SESION */
    exit;
?>