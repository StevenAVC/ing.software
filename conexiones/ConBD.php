<?php

    /* CONEXION A LA BASE DE DATOS */
    define('DB_SERVIDOR', 'localhost');
    define('DB_USUARIO', 'root');
    define('DB_PASS', '');
    define('DB_NOMBRE', 'pijamas');

    $conexion = mysqli_connect(DB_SERVIDOR, DB_USUARIO, DB_PASS, DB_NOMBRE);

    /* MENSAJE ERROR DE LA BASE DE DATOS*/
    if($conexion === false){
        die("ERROR A LA HORA DE CONECTAR CON LA BASE DE DATOS " . mysqli_connect_error());
    }
?>