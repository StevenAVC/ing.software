$(function(){
    validacionCarrito();
});

function validacionCarrito(){
    $("#carritoForm").validate({
        rules:{
            id_pijama: {
                required: true
            },
            email: {
                required: true,
                email: true,
                maxlength: 100
            },
            telefono: {
                required: true,
                maxlength: 100
            },
        },
    });
}