<?php
    /* INICIAR SESIÓN */
    session_start();

    /* METODO PARA ENTRAR SOLO CON INICIO DE SESION */
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: index.php");
        exit;
}

?>

<!DOCTYPE html>

<html> 
    <head> 
        <?php include("./MenuNav.php") ?>
        <link rel="stylesheet" href="css/footer.css">
        <script src="https://kit.fontawesome.com/07570749ac.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    </head>
    <body> 
        <?php require_once('ConDB.php');  ?>
         <?php require_once('ComentariosProcesos.php');  ?>
         
        <?php if(isset($_SESSION['mensaje'])):?>
        <div class="alert alert-success" >
            <?php
                echo $_SESSION['mensaje'];
                unset( $_SESSION['mensaje']);
            ?>
            
            <?php 
                endif;
            ?>
        </div>
        <div style="margin-left:30px; margin-right:30px;">	
            <br>
            <h2> Contáctenos </h2>
            <br>
            <form action="ComentariosProcesos.php" method="post">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <label for="Nombre">Nombre: </label>
                            <input class="form-control" id="nombre" name="nombre" type="Text">
                            <!-- <input id="Nombre" type="Text"> -->
                        </div>					

                        <div class="col-md-6 col-lg-4">
                            <label for="Email">Email: </label>
                            <!-- <input id="Email" type="Text"> -->
                            <input name="email" class="form-control" id="email" type="Text">
                        </div>

                        <div class="col-md-10">
                            <label for="Asunto">Asunto: </label>
                            <textarea name="asunto" class="form-control" id="asunto"> </textarea>
                            <!-- <textarea id="Asunto"> </textarea> -->
                        </div>

                        <div class="col-md-6 col-lg-4">
                            <br>
                            <button class="btn btn-outline-primary" name="btnEnviar" id="btnEnviar">Enviar</button>
                        </div>
                    </div>
                    <br>
                    <?php $comentarios = $mysqlconn->query("SELECT * FROM comentarios") or die($mysqlconn->error) ?>

                    <h3>Comentarios</h3>
                    <div class="row">
                        <table class="table table-bordered table-dark">
                            <tr>
                                <th>N Cliente</th> 
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Comentario</th>
                                <th>Eliminar Comentario</th>
                            </tr> 
                            <?php while ($dato = $comentarios->fetch_assoc()): ?>
                                <tr>
                                    <td> <?php echo $dato['id'] ?> </td>
                                    <td> <?php echo $dato['nombre'] ?> </td>
                                    <td> <?php echo $dato['email'] ?> </td>
                                    <td> <?php echo $dato['asunto'] ?> </td>
                                    <td>
                                        <a class="btn btn-outline-danger" href="ComentariosProcesos.php?borrar=<?php echo $dato['id'] ?>" >Eliminar Comentario</a>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        </table>
                    </div>
                    <br>
                    <h4>Ubicación</h4>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15719.983330650868!2d-84.075915!3d9.934304!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb56e80e509753e48!2sUniversidad%20Castro%20Carazo!5e0!3m2!1ses!2scr!4v1619331620475!5m2!1ses!2scr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>

            </form>
        </div> 
        <br>   
<?php
    include('footer.html');
?>  
    </body>
</html>